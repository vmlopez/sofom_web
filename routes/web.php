<?php

use App\Models\Event;

Route::get('/','WebsiteController@index');
Route::get('/soluciones','WebsiteController@dealers');
Route::get('/nosotros','WebsiteController@workWithUs');
Route::get('/gana-mas' , 'WebsiteController@earnMore');
Route::post('/gana-mas' , 'WebsiteController@sendContactMail');
//Route::get('/formatos' , 'WebsiteController@formats');
Route::get('/regulaciones' , 'WebsiteController@regulations');
Route::get('/cat', 'WebsiteController@cat');
Route::get('/atencion', 'WebsiteController@userAtention');
Route::get('/aviso-de-privacidad', 'WebsiteController@privacyNote');
Route::get('/buro-entidades-financieras', 'WebsiteController@finantialEntities');
// Rutas para industrias
//Route::get('/industrias/avicultura','WebsiteController@avicultura');
//Route::get('/industrias/porcicultura','WebsiteController@porcicultura');
//Route::get('/industrias/bovino','WebsiteController@bovino');
//Route::get('/industrias/camaron','WebsiteController@camaron');
//Route::get('/industrias/peces','WebsiteController@peces');
//Route::get('/industrias/equinos','WebsiteController@equinos');
//Route::get('/industrias/fosfato','WebsiteController@fosfato');

//Rutas para acerca de nosotros
//Route::get('/nosotros/quienes-somos','WebsiteController@about');
//Route::get('/nosotros/certificaciones','WebsiteController@certifications');
//Route::get('/nosotros/infraestructura','WebsiteController@infrastructure');
//Route::get('/nosotros/investigacion','WebsiteController@investigation');

//Rutas de autenticación
//Auth::routes();


