<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Requests\StoreEvent;

class EventController extends Controller
{
    //
	
	
	public function show(Event $event)
	{
		return view('admin.events.showEvent',compact('event'));
	}
    public function store(StoreEvent $request)
    {
    	$start = getCarbonDate($request->start);
    	$end   = getCarbonDate($request->end);

    	//Validación de las fechas
    	$invalidStart  = Event::whereBetween('start' ,[$start,$end])
    					->where('room_id',$request->room_id)
    					->get();
    	$invalidEnd	   = Event::whereBetween('end' ,[$start,$end])
    					->where('room_id',$request->room_id)
    					->get();

    	if($invalidEnd->first() || $invalidStart->first())
    	{
    		$request->session()->flash('error', 'Ya hay un apartado para la sala que seleccionaste');
			return redirect('/admin/calendario');
		}
		if($request->start == $request->end || $start > $end)
		{
			$request->session()->flash('error', 'El rango de fechas que seleccionaste no es válido');
			return redirect('/admin/calendario');
		}
		// Terminan validaciones de fecha

    	$event = new Event($request->all());

    	$event->user_id = Auth::user()->id;
    	$event->start 	= $start;
    	$event->end 	= $end; 

    	$event->save();

    	$event->url =  '/admin/calendario/evento/'.$event->id;

    	$event->save();
    	
    	$request->session()->flash('status', 'Evento creado exitosamente');

    	return redirect('/admin/calendario');
    	
    }


    
}


function getCarbonDate( $date )
{
	$date = str_replace('-', ",", $date);
	$date = new Carbon($date);
	return ($date);
}