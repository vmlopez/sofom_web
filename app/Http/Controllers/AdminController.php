<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    //
    public function home()
    {
    	return view('admin.home');
    }
    public function rooms()
    {
    	return view('admin.rooms');
    }

    public function store()
    {
    	return view('admin.rooms.store');
    }
}
