<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use function foo\func;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class WebsiteController extends Controller
{
    public function index()
    {
    	return view('website.index');
    }

    public function dealers()
    {
    	return view('website.dealers');
    }

    public function workWithUs()
    {
    	return view('website.workWithUs');
    }

    public function earnMore( Request $request )
    {
        if( $request->query('contacto') != null )
            $contact = true;
        else
            $contact = false;

        return view('website.earnMore', compact('contact'));
    }

    public function sendContactMail( Request $request )
    {
        $data = [
            'name'      => $request->name,
            'location'  => $request->location,
            'phone'     => $request->phone,
            'email'     => $request->email,
            'comments'  => $request->comments
        ];

        Mail::send('emails.contact', ["data" => $data ] , function ($message){
            $message->to('lopez.victor94@gmail.com');
            $message->from('contacto@sofom.vimifos.com');
            $message->subject('Contacto nuevo desde website');
        });

        $success = true;
        $contact = false;

        return view('website.earnMore', compact('success','contact'));

    }

    public function finantialEntities()
    {
        return view('website.finantialEntities');
    }

    public function formats()
    {
        return view('website.formats');
    }

    public function regulations()
    {
        return view('website.regulations');
    }

    public function cat()
    {
        return view('website.cat');
    }

    public function userAtention()
    {
        return view('website.userAtention');
    }

    public function privacyNote()
    {
        return view('website.privacyNote');
    }

    // Funciones para industrias
    public function avicultura()
    {
    	return view('website.industries.avicultura');
    }
    public function bovino()
    {
    	return view('website.industries.bovino');
    }
    public function camaron()
    {
    	return view('website.industries.camaron');
    }
    public function equinos()
    {
    	return view('website.industries.equinos');
    }
    public function fosfato()
    {
    	return view('website.industries.fosfato');
    }
    public function peces()
    {
    	return view('website.industries.peces');
    }
    public function porcicultura()
    {
    	return view('website.industries.porcicultura');
    }
    // Terminan funciones para industria


    // Acerca de nosotros
    public function about()
    {
    	return view('website.aboutUs.about');
    }
    public function certifications()
    {
    	return view('website.aboutUs.certifications');
    }
    public function investigation()
    {
    	return view('website.aboutUs.investigation');
    }
    public function infrastructure()
    {
    	return view('website.aboutUs.infrastructure');
    }
    // Terminan funciones acerca de nosotros
}
