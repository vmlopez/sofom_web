<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $fillable = 
    [
        'user_id',
        'room_id', 
        'title',
        'all_day',
		'start',
		'end',
		'url',
		'class_name',
		'editable',
		'start_editable',
		'duration_editable',
		'resource_editable',
		'color',
		'background_color',
		'corder_color',
		'text_color'
    ];

    public function room()
    {
    	return $this->belongsTo('App\Models\Room');
    }
    public function user()
    {
    	return $this->belongsTo('App\Models\User');
    }
}
