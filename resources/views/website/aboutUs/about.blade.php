@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-camaron" role="tablist">
					<li role="presentation" class="active">
						<a href="#quienes-somos" aria-controls="quienes-somos" role="tab" data-toggle="tab">QUIENES SOMOS</a>
					</li>
					<li role="presentation">
						<a href="#mision" aria-controls="mision" role="tab" data-toggle="tab">MISIÓN Y VISIÓN</a>
					</li>
					<li role="presentation">
						<a href="#historia" aria-controls="historia" role="tab" data-toggle="tab">HISTORIA</a>
					</li>
					<li role="presentation">
						<a href="#valores" aria-controls="valores" role="tab" data-toggle="tab">VALORES</a>
					</li>
					<li role="presentation">
						<a href="#financiamiento" aria-controls="financiamiento" role="tab" data-toggle="tab">FINANCIAMIENTO</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="quienes-somos">
						<div class="row margin-top-15">
							<div class="col-md-8 ">
								<h3>QUIENES SOMOS</h3> <br>
								<p class="text-justify blue">
									<b>Somos una empresa con más de 35 años de experiencia, líder en la Industria de la Nutrición Animal en México. Con verdadera pasión y entrega, nuestra filosofía de Creación de Valor nos impulsa a brindar soluciones innovadoras que mejoren la productividad de nuestros clientes.</b> 
								</p>
								<br>
								<p class="text-justify ">
									Los productos elaborados por VIMIFOS cuentan con el respaldo de personal altamente capacitado y con Sistemas de Gestión de Calidad que permiten garantizar un excelente desempeño.
									<br><br>
									A través de nuestra alianza con ADM Alliance Nutrition, contamos con un respaldo de más de 100 años en investigación y desarrollo de tecnología para la nutrición animal.
									<br><br>
									Con VIMIFOS Capital, nuestra entidad financiera, hemos logrado colocar anualmente apoyos económicos que han contribuido al incremento en la productividad de las distintas industrias que servimos.
									<br>
									
								</p>
									
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/nosotros/acerca/quienessomos.jpg"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="mision">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3>NUESTRA MISIÓN</h3> <br>
								<p class="text-justify blue">
									<b>"Somos una organización con espíritu emprendedor y de servicio, que contribuye a la prosperidad de nuestros clientes.“</b> 
								</p>
								<br>
								<h3>NUESTRA VISIÓN</h3> <br>
								<p class="text-justify blue">
									<b>"Trascender con pasión y orgullo como la organización más confiable que contribuye al éxito y prosperidad de nuestros clientes, mediante soluciones innovadoras, globales y sustentables."</b> 
								</p>
								
								
								<br>
								
							</div>
							<div class="col-md-4">
								<img src="/img/nosotros/acerca/mision.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="historia">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5>NUESTRA HISTORIA</h5>
								<p>
									<b class="blue">1977</b> • VIMIFOS se creó por iniciativa de productores pecuarios del Estado de Sonora con el objetivo de producir premezclas de Vitaminas y Minerales de excelente calidad. <br><br>
	 
									<b class="blue">1983</b> • Se amplía la gama de productos y se aprueba la construcción de una planta de fosfatos que garantiza 100% de disponibilidad biológica. Esta nueva planta fue el pilar para iniciar la participación comercial en todo el país, logrando el reconocimiento de nutriólogos y productores siendo la primera empresa que producía un fosfato. <br><br>
									 
									<b class="blue">1992</b> • VIMIFOS se asocia con ADM  en su división de Nutrición y Salud Animal. Esta asociación le permitió mayor respaldo científico y tecnológico así como continuar con un crecimiento sostenido. <br><br>
									 
									<b class="blue">1994</b> • Se inicia la comercialización de alimentos para camarón representando a una firma extranjera. <br><br>
									 
									<b class="blue">1995</b> • Se construye una nueva planta de Premezclas en la ciudad de Guadalajara, Jalisco, México. <br><br>
									 
									<b class="blue">1999</b> • Comienza la producción de alimentos para camarón, logrando un crecimiento sostenido hasta lograr el liderazgo actual. <br><br>
									 
									<b class="blue">2007</b> • Apertura de la nueva planta de premezclas en El Salto, Jalisco siendo la más sofisticada y con mayor tecnología en Latinoamérica, al mismo tiempo arranca operaciones la nueva planta de Alimentos Terminados (PATCCO) en Ciudad Obregón, Sonora. <br><br>
									 
									<b class="blue">2010</b> • Comienza la construcción de la nueva planta de Especialidades en El Salto, Jalisco para incursionar en industrias en lo que anteriormente no se tenía participación.  <br><br> 
									 
									<b class="blue">2011</b> • Se obtienen la certificación HACCP en planta de premezclas de El Salto, Jalisco. <br><br>
									 
									<b class="blue">2012</b> • VIMIFOS cumple 35 años al servicio de toda la industria Pecuaria y Acuícola en México, ofreciendo además de productos y servicios, herramientas tecnológicas que promueven la productividad de sus clientes. Lanzamos la primera aplicación en México para el cultivo del camarón. <br><br>
								</p>							

							</div>
							<div class="col-md-4">
								<img src="/img/nosotros/acerca/historia.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="valores">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<br>
								<h5 class="blue">NUESTROS VALORES</h5>
								
								<p class="text-justify ">
									Dentro de nuestra filosofía empresarial, creemos firmemente que para lograr la prosperidad debemos conocer con todo detalle a nuestros clientes, entender sus necesidades y entregar propuestas de valor que les permitan resolver sus requerimientos alcanzando su propia prosperidad. Ayudarlos a lograr sus metas de negocio, es un compromiso de todas las áreas al interior de la empresa; por ello buscamos lograr su satisfacción a través de todos los recursos que tenemos a nuestra disposición. <br><br>

									Para VIMIFOS, realizar una venta no es el objetivo, es solo un síntoma de la comunicación exitosa. Es un síntoma que nos indica que comprendemos a nuestros clientes y que medimos nuestro éxito en base a su triunfo.  
								</p>
								
								
								<p class="blue">Nuestros Valores</p>
								<br>
								<p class="light-blue">INTEGRIDAD:</p>
								<p>Conducirse con honestidad, respeto y congruencia.</p>
								<br>
								<p class="light-blue">ESPÍRITU DE SERVICIO:</p>
								<p>Disponibilidad para actuar  anticipadamente a las necesidades de los demás.</p>
								<br>
								<p class="light-blue">INNOVACIÓN:</p>
								<p>Capacidad de ver nuevas opciones y hacer algo al respecto.</p>
								<br>
								<p class="light-blue">COMPROMISO:</p>
								<p>Cumplir responsablemente superando expectativas con la empresa y nuestros clientes.</p>
								<br>
								<p class="light-blue">TENACIDAD:</p>
								<p>Atreverse a hacer consistentemente cosas diferentes. No tener miedo al fracaso, no rendirse.</p>
								<br>
								<p class="light-blue">SUPERACIÓN:</p>
								<p>Actitud de logro.</p>
								<br>
								<p class="light-blue">APERTURA A LA DIVERSIDAD:</p>
								<p>Disposición para aceptar diferentes formas de ser y de pensar. </p>
								<br>
								

								
								
							</div>
							<div class="col-md-4">
								<img src="/img/nosotros/acerca/valores.png"  class="img-100 pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="financiamiento">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="blue">FINANCIAMIENTO</h5>
								<br>

								<p class="text-justify blue">
									<b>
									Desde el 2005 servimos como plataforma para ampliar la capacidad empresarial de los productores.
									</b>
								</p>

								
								<p class="text-justify">
									<b>
										VIMIFOS CAPITAL es nuestra institución financiera que cree fielmente en el sector Pecuario y Acuícola mexicano, apoyando a productores pequeños, medianos y grandes por medio de variados instrumentos de crédito y financiamiento.
									</b>
									<br><br>
									Somos el intermediario financiero que apoya el sector Pecuario y Acuícola en México. Sabemos que cada negocio es muy particular y opera de forma diferente por lo que creamos créditos a la medida de sus necesidades. Buscamos involucrarnos de manera profunda en su negocio con el objetivo de detectar sus necesidades y poder ofrecer soluciones efectivas a los problemas que enfrenta cotidianamente.
								</p>
								
								<br>

								<p>
									Nos especializamos en las siguientes industrias:
								</p>
								<p  class="light-blue">
									- ACUICULTURA. <br><br>

									- GANADERÍA. <br><br>

									- PORCICULTURA. <br><br>

									- AVICULTURA. <br><br>
								</p>

								

								<p class="text-justify">	
									Contáctanos: <br><br>
									O1 800 00 VIMCAP (846 227)
									<br><br>	
									financiamiento@vimifos.com
									<br><br>
								</p>

							</div>
							<div class="col-md-4">
								<img src="/img/nosotros/acerca/financiamiento.png"  class="img-100 pull-right" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


