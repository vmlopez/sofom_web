@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-12">

                <h4>REGULACIONES</h4>

                <br>

                <p class="blue">
                    Vimifos Capital es una Sociedad Financiera de Objeto Múltiples autorizada para su operación
                    y supervisada por la Comisión Nacional para la Protección y Defensa de los Usuarios de Servicio Financieros.
                </p>
                <p class="text-justify">
                    <br>


                    Corporativo Financiero Vimifos, S. A. de C. V., SOFOM, E. N. R., fue constituida el 24 de noviembre
                    del 2005, en Guadalajara, Jalisco como Corporativo Vimifos S. A. de C. V. Fue autorizada en febrero
                    de 2006 y se publicó en el Diario Oficial de la Federación en marzo de  2006 como Corporativo Financiero
                    Vimifos, S. A. de C. V., Sociedad Financiera de Objeto Limitado  (SFOL) y en noviembre de 2012 se
                    transformó a Sociedad Financiera de Objeto Múltiple (SOFOM)Entidad no Regulada E.N.R.

                    <br><br>

                    El 06 de noviembre de 2012 la compañía presentó ante el Servicio de Administración Tributaria (SAT)
                    el cambio de régimen de capital de Sociedad Financiera de Objeto Limitado (SFOL) a Sociedad Financiera
                    de Objeto Múltiple (SOFOM) E.N.R., adicionalmente en la misma fecha la compañía dejó de ser entidad regulada,
                    sin embargo, de acuerdo a la política interna seguimos aplicando las disposiciones conforme a los
                    criterios contables establecidos por la CNVB y a las NIF Mexicanas. Asimismo se sigue dando cumplimiento
                    de las obligaciones a las que estamos sujetos ante las diversas autoridades.

                    <br><br>


                    <b>SHCP - Secretaría de Hacienda y Crédito Público.</b>

                    <br><br>

                    De acuerdo a la política interna aplicamos las disposiciones conforme a los criterios contables
                    establecidos por la CNVB y a las NIF Mexicanas. Asimismo dando cumplimiento de las obligaciones a
                    las que estamos sujetos ante las diversas autoridades.

                    <br><br>


                    <b>CNBV - Comisión Nacional Bancaria y de Valores.</b>

                    <br><br>

                    La Comisión Nacional Bancaria y de Valores supervisa que las Sociedades Financieras de Objeto Múltiple
                    (SOFOMES), cumplamos con las disposiciones establecidas por la SHCP en materia de prevención de lavado de dinero.

                    <br><br>

                    <b>FIRA Banco de México- Fideicomisos Instituidos en Relación con la Agricultura.</b>

                    <br><br>

                    Somos Intermediarios Financieros de éste organismos, es nuestro principal proveedor
                    de recursos, por lo que estamos obligados a cumplir con sus condiciones generales de
                    operación. Estamos sujetos a revisiones continuas,  donde verifican su cumplimiento.
                    Otorgamos a nuestros clientes los beneficios de sus programas de crédito, así como la
                    capacitación, asistencia técnica, transferencia de tecnología, optimización de sus granjas, etc.

                    <br><br>

                    <b>CONDUSEF- Comisión Nacional para la Protección y Defensa de los Usuarios de Servicios Financieros.</b>

                    <br><br>

                    Estamos sujetos a cumplir con las disposiciones que establece este organismo, principalmente nos supervisa
                    el contenido de los contratos que celebramos con nuestros clientes, las comisiones que cobramos por servicios,
                    nos obliga a  constituir una unidad especializada de atención a usuarios para recibir reclamaciones, quejas y
                    atender consultas relacionadas con nuestros servicios financieros y retroalimentar a dicho organismo de las mismas.
                    Está facultado para aplicarnos sanciones económicas por el incumplimiento de las reglas.

                    <br><br>

                    <b>AMFE – Asociación Mexicana de Entidades Financieras Especializadas.</b>

                    <br><br>

                    Al ser miembros de este organismo estamos sujetos a presentar la información financiera de forma mensual,
                    que a su vez la publican en su página de internet, con la finalidad de enterar a al público interesado de
                    nuestra situación financiera y de las demás sociedades que se dedican a la misma actividad.

                    <br><br>

                    <b>OFICINAS Av. Lázaro Cárdenas 3430 piso 3, Int. 303 - 304, Col. Jardines de los Arcos, Guadalajara, Jalisco, C.P. 44500
                    </b>

                </p>

            </div>


        </div>
    </div>

@endsection



@section('footer')
    @parent
@endsection

@section('scripts')



@endsection


