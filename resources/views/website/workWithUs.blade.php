@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs " role="tablist">
					<li role="presentation" class="active">
						<a href="#nosotros" aria-controls="nosotros" role="tab" data-toggle="tab">
							QUIENES SOMOS
						</a>
					</li>
					<li role="presentation">
						<a href="#mision-y-vision" aria-controls="mision-y-vision" role="tab" data-toggle="tab">
							MISIÓN Y VISIÓN
						</a>
					</li>
					<li role="presentation">
						<a href="#historia" aria-controls="historia" role="tab" data-toggle="tab">
							HISTORIA
						</a>
					</li>
					<li role="presentation">
						<a href="#valores" aria-controls="valores" role="tab" data-toggle="tab">
							VALORES
						</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="nosotros">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4 class="blue">QUIÉNES SOMOS</h4>
								<h4 class="blue"><b>Somos el intermediario financiero no bancario con apoyos al sector Pecuario y Acuícola.</b></h4>
								<p class="text-justify">
									Sabemos que cada negocio es muy particular y opera de forma diferente por lo que
									creamos créditos a la medida de tus necesidades. Buscando involucrarnos de manera
									profunda en tu negocio con el objetivo de detectar tus áreas de oportunidad y poder
									ofrecer soluciones efectivas a los problemas que enfrentan cotidianamente.
									<br><br>
									Nuestra experiencia y el respaldo de VIMIFOS nos da una fortaleza difícilmente
									alcanzable por otra institución del ramo, y es que conocemos a la perfección el
									mercado,  por lo que ofrecemos créditos a la medida de cada cliente.
									<br><br>
									Conozca lo que te ofrecemos para incrementar la productividad de tu negocio.

								</p>

							</div>
							<div class="col-md-4">
								<img src="/img/trabaja-con-nosotros/banner_quienessomos.jpg"  class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="colaboradores">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="blue">COLABORADORES</h3>
								<h4 class="blue"><b>En VIMIFOS nuestra gente es la clave del éxito</b></h4>
								<p class="text-justify">
									<b>Han sido años de dedicación, esfuerzo y compromiso que nos han recompenzado con grandes éxitos, en la industria, impulsados por cada uno de nuestros colaboradores que nos llenan de satisfacción.</b>
								</p>
								<br>
								<p class="text-justify">
									En <b>VIMIFOS</b> somos cientos de clientes colaboradores, trabajando en las diferentes localidades. Nuestro trabajo toca la vida de millones de personas cada día, elaborando productos que intervienen de forma directa en la cadena alimenticia del ser humano, lo que conlleva una gran responsabilidad. Por ello nuestro activo más valioso es nuestro equipo. Por esta razón nos esforzamos en seleccionar al personal más competitivo, para juntos lograr los objetivos que nos planteamos como compañía.
								</p>
								<br>
								<p class="text-justify">
									<b>VIMIFOS</b> es el lugar donde todos estemos orgullosos de trabajar. Es donde se practican los valores todos los días, en cada uno de nuestros actos, es donde se aporta la calidad y excelencia, logrando seguir alcanzando los resultados correctos de manera correcta.
								</p>

								<h3 class="blue">En VIMIFOS</h3>
								<br>
								<ul class="text-justify">
									<li>Reconocemos la dignidad de las personas.</li>
									<li>Creemos y respetamos su libertad y privacidad.</li>
									<li>Consideramos que la salud y seguridad son tan importantes como cualquier otra función y objetivo de la campaña</li>
								</ul>
								<br>
								<p><b>El éxito de cada proyecto tiene como único sustento la participación de todos los colaboradores</b></p>
								<br>
								<p class="blue">Juntos día a día creamos una empresa con valor</p>
							</div>
							<div class="col-md-4">
								<img src="/img/trabaja-con-nosotros/banner-colaboradores.jpg"  class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="mision-y-vision">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4>NUESTRA MISIÓN</h4>
								<h4 class="blue">"Somos una organización con espíritu emprendedor y de servicio,
									que contribuye a la prosperidad de nuestros clientes.“</h4>
								<br>

								<h4>NUESTRA VISIÓN</h4>
								<h4 class="blue">"Trascender con pasión y orgullo como la organización más confiable
									que contribuye al éxito y prosperidad de nuestros clientes, mediante soluciones innovadoras, globales y sustentables."
								</h4>


							</div>
							<div class="col-md-4">
								<img src="/img/trabaja-con-nosotros/banner-misionyvision.jpg"  class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane padding-5 " id="historia">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4 >HISTORIA</h4>
								<h4 class="blue">
									VIMIFOS CAPITAL es una institución financiera que cree fielmente en el sector
									agropecuario mexicano, apoyando a productores pequeños, medianos y grandes por medio de variados instrumentos de  financiamiento.
								</h4>
								<br><br><br>

								<p class="text-justify">
									Contando con el respaldo de VIMIFOS con más de 35 años de experiencia en el medio.
									<br><br>
									Así como el respaldo de nuestros accionistas que son algunos de los principales productores agropecuarios y acuícolas del país.
									<br><br>
									Desde el 2006 servimos como plataforma para ampliar la capacidad empresarial de los productores, teniendo una de las mejores calificaciones de riesgo de la industria en nuestro sector.
									<br><br>
									Garantizando un compromiso apoyando a más de 180 clientes, traduciéndose en más de $1,000 MDP en créditos operados por año.
									<br><br>
								</p>
							</div>
							<div class="col-md-4">
								<img src="/img/trabaja-con-nosotros/banner-historia.jpg" class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>

					<div role="tabpanel" class="tab-pane padding-5 " id="valores">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4 >NUESTROS VALORES</h4>
								<p class="text-justify">
									Dentro de nuestra filosofía empresarial, creemos firmemente que para lograr la prosperidad debemos
									conocer con todo detalle a nuestros clientes, entender sus necesidades y entregar propuestas de valor
									que les permitan resolver sus requerimientos alcanzando su propia prosperidad. Ayudarlos a lograr sus
									metas de negocio, es un compromiso de todas las áreas al interior de la empresa; por ello buscamos
									lograr su satisfacción a través de todos los recursos que tenemos a nuestra disposición.
								</p>

								<h4 class="blue">
									Nuestros Valores:
								</h4>

								<br>

								<p><b class="light-blue">INTEGRIDAD:</b> Conducirse con honestidad, respeto y congruencia.</p>
								<br>
								<p><b class="light-blue">ESPÍRITU DE SERVICIO:</b> Disponibilidad para actuar  anticipadamente a las necesidades de los demás.</p>
								<br>
								<p><b class="light-blue">INNOVACIÓN:</b> Capacidad de ver nuevas opciones y hacer algo al respecto.</p>
								<br>
								<p><b class="light-blue">COMPROMISO:</b> Cumplir responsablemente superando expectativas con la empresa y nuestros clientes.</p>
								<br>
								<p><b class="light-blue">TENACIDAD:</b> Atreverse a hacer consistentemente cosas diferentes. No tener miedo al fracaso, no rendirse.</p>
								<br>
								<p><b class="light-blue">SUPERACIÓN:</b> Actitud de logro.</p>
								<br>
								<p><b class="light-blue">APERTURA A LA DIVERSIDAD:</b> Disposición para aceptar diferentes formas de ser y de pensar.</p>

							</div>
							<div class="col-md-4">
								<img src="/img/trabaja-con-nosotros/banner-valores.jpg" class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


