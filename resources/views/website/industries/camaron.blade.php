@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-camaron" role="tablist">
					<li role="presentation" class="active">
						<a href="#camaron" aria-controls="camaron" role="tab" data-toggle="tab">CAMARÓN</a>
					</li>
					<li role="presentation">
						<a href="#introduccion" aria-controls="introduccion" role="tab" data-toggle="tab">INTRODUCCIÓN</a>
					</li>
					<li role="presentation">
						<a href="#productos" aria-controls="productos" role="tab" data-toggle="tab">PRODUCTOS</a>
					</li>
					<li role="presentation">
						<a href="#tecnologias" aria-controls="tecnologias" role="tab" data-toggle="tab">TECNOLOGÍAS</a>
					</li>
					<li role="presentation">
						<a href="#alianzas" aria-controls="alianzas" role="tab" data-toggle="tab">ALIANZAS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="camaron">
						<div class="row margin-top-15">
							<div class="col-md-12">
								{{-- <img class="center-block" src="/img/industrias/avicultura/tab-1.png" alt=""> --}}
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="introduccion">
						<div class="row margin-top-15">
							<div class="col-md-8 camaron-color">
								<h3 class="camaron-color">INTRODUCCIÓN</h3> <br>
								<p class="text-justify camaron-color">
									<b>Una alianza se forma para mejorar, optimizar la producción y juntos obtener el fruto deseado... el éxito.</b> 
								</p>
								<br>
								<p class="text-justify camaron-color">
									Para VIMIFOS apostar en el futuro de la industria es una prioridad, así como la creación de cadenas de valor que fortalezcan la productividad del sector acuícola y en específico del camarón. Generar alianzas con nuestros clientes que mejoren su productividad a través de productos basados en nutrición científicamente formulada, capaces de obtener un máximo desarrollo en su cultivo, nos distingue. Además de contar con un departamento de servicios técnicos que otorga asesoría de primer nivel que detona oportunidades.
									<br><br>
									Elaboramos nuestros productos con el tamaño requerido de acuerdo a la etapa del ciclo de vida del camarón, desde la post-larva hasta la cosecha, iniciando con Migajas, Starter, Micropellet y Pellet Tanto Starter como Micropellet son desarrollos industriales únicos de VIMIFOS.
									<br><br>
									Todos los productos están elaborados en las dósis necesarias para cubrir los requerimientos de cada fase de alimentación, ofreciendo las siguientes ventajas.
									<br>
									<ul>
										<li class="camaron-color"><b>Alta digestibilidad</b></li>
										<p>Formulas diseñadas con una alta inclusión de proteina de alta digestibilidad</p>
										<li class="camaron-color">Nutrientes balanceados</li>
										<p>La selección cuidadosa de ingredientes nos permite ofrecer alimentos de alta calidad cuya eficiencia ha sido mejorada por la adición de atrayentes naturales.</p>
									</ul>
								</p>
								<p>
									
								</p>
								<small>
									El paquete de productos y servicios disponibles para la Ganadería están soportados por VIMIFOS con un excelente servicio técnico en las áreas de nutrición, manejo, sanidad, aseguramiento de calidad, reproducción y análisis de la información, así como también por asesores externos tanto nacionales como extranjeros.
								</small>
								<br>
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/camaron/introduccion.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="productos">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/camaron/productos/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											La alternativa de nutrición más avanzada en el mercado mexicano, especial para los acuicultores que están a la vanguardia. 
											<b>	Máxima digestibilidad, con atrayentes naturales de origen marino y elevados niveles de vitaminas y minerales. </b><br>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/camaron/productos/2.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Solución que se adapta a tus necesidades, con la cual se han obtenido hasta 6 TM/ha, con camarones de hasta 34g, presentando un excelente factor de Conversión Alimenticia. <br>	
											<b>Alta digestibilidad, con atrayentes naturales y un excelente balance de nutrientes.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/camaron/productos/3.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Solución que se adapta a tus necesidades, con la cual se han obtenido hasta 6 TM/ha, con camarones de hasta 34g, presentando un excelente factor de Conversión Alimenticia. <br>	
											<b>Alta digestibilidad, con atrayentes naturales y un excelente balance de nutrientes.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/camaron/productos/4.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Cuidadosamente formulado con nutracéuticos, además de nutrir al camarón optimiza y restaura su salud de una manera tanto preventiva como correctiva. <br>	
											<b>	Optimiza y restaura la salud.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/camaron/productos/5.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											La mejor dieta para los sistemas de maternidades y/o Raceway de camarón. Contiene todo lo necesario para la primer etapa de una gran producción. <br>	
											<b>	Un buen inicio siempre será la diferencia.</b>
										</p>
									</div>
								</div>

								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/camaron/productos/6.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Especial para reproductores, ideal para etapa de engorda, óptimo crecimiento de los reproductores producidos en laboratorio de ciclo cerrado. <br>	
											<b>	Alimento especializado para reproductores.</b>
										</p>
									</div>
								</div>
								
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/camaron/productos.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tecnologias">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="camaron-color">TECNOLOGÍAS</h5>
								<br><br>	
								<h4>Nuevo DuoPack Nutricional, diseñado para optimizar su producción.</h4>
								<br><br>	
								<li class="camaron-color"><b>Power Armor: </b></li>
								<p class="text-justify camaron-color">
									Un Mix Nutricional completo formulado a base de Megadosis de Vitaminas C y E, mananoligosacáridos y paredes celulares de levadura altas en betaglucanos . Actúan como inmunomodulador ayudando a controlar los patógenos, mediante el fortalecimiento del sistema inmune del camarón.
								</p>
								<br><br>	
								<li class="camaron-color"><b>Zero Xchange: </b></li>
								<p class="text-justify camaron-color">
									Paquete a base de mezclas de aminoácidos libres cuya función, es ayudar  al camarón a gastar menos energía en su proceso de regulación osmótica al estar en situación de alta densidad.Ideal para estanques cerrados por cuestiones sanitarias o porque la fuente de agua es de alta salinidad desde su origen.
								</p>

								

								<div class="row">	
									<div class="col-md-6">
										<img src="/img/industrias/camaron/tecnologias/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-6">
										<img src="/img/industrias/camaron/tecnologias/2.png" class="center-block" alt="">
									</div>
								</div>
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/camaron/tecnologias.png"  class="img-100 pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="alianzas">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="camaron-color">ALIANZAS</h5>
								<br>

								<li class="camaron-color">ADM:</li>
								<p class="text-justify camaron-color">
									<b>
									Contamos con alianzas estratégicas que nos ayudan a elaborar productos únicos que ayuden a obtener mayor productividad en los negocios de nuestros clientes.
									</b>
								</p>

								
								<p class="text-justify">
									Archer Daniels Midland Company es uno de los más grandes procesadores agrícolas del mundo. Fundada en 1902 e incorporada en 1923. ADM tiene su sede en DECATUR, su operación es en todo el mundo a través de sus amplias instalaciones de distribución global. <br><br>
									ADM hace una contribución significativa a la economía mundial y la calidad de vida su división de nutrición y salud animal permite brindar un mayor respaldo científico y tecnológico gracias a un gran equipo de bioquímicos que trabajan para crear productos que agregarán valor y beneficios importantes a nuestros clientes. <br><br>	
								</p>

								<li class="camaron-color">INTEGRATED AQUA CULTURE INTERNATIONAL:</li>

								<p class="text-justify">	
									De la misma forma se trabaja en colaboración con la empresa Integrated Aquaculture International, la cual opera instalaciones de investigación en nutrición y genética de organismos acuáticos en Hawaii, Brunei, India y China.
									<br><br>	
									En México se mantienen convenios de colaboración con los principales productores acuícolas para pruebas de campo en sus instalaciones que permitan la mejora continua de las dietas y su adecuación al entorno local.
								</p>
								<div class="col-md-6">	
									<img src="/img/industrias/porcicultura/adm.png" class="center-block margin-top-15" alt="">
								</div>
								<div class="col-md-6">	
									<img src="/img/industrias/camaron/alianzas/2.png" class="center-block margin-top-15" alt="">
								</div>
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/porcicultura/alianzas.png"  class="img-responsive pull-right" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


