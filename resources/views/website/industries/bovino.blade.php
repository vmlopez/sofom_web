@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-bovino" role="tablist">
					<li role="presentation" class="active">
						<a href="#bovino" aria-controls="bovino" role="tab" data-toggle="tab">BOVINO</a>
					</li>
					<li role="presentation">
						<a href="#introduccion" aria-controls="introduccion" role="tab" data-toggle="tab">INTRODUCCIÓN</a>
					</li>
					<li role="presentation">
						<a href="#productos" aria-controls="productos" role="tab" data-toggle="tab">PRODUCTOS</a>
					</li>
					<li role="presentation">
						<a href="#tecnologias" aria-controls="tecnologias" role="tab" data-toggle="tab">TECNOLOGÍAS</a>
					</li>
					<li role="presentation">
						<a href="#alianzas" aria-controls="alianzas" role="tab" data-toggle="tab">ALIANZAS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="bovino">
						<div class="row margin-top-15">
							<div class="col-md-12">
								{{-- <img class="center-block" src="/img/industrias/avicultura/tab-1.png" alt=""> --}}
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="introduccion">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="bovino-color">INTRODUCCIÓN</h3> <br>
								<p class="text-justify bovino-color">
									<b>En la división de Ganadería estamos dedicados a desarrollar productos y servicios estratégicos que puedan ayudar a mejorar la rentabilidad de las explotaciones ganaderas de nuestros clientes</b> 
								</p>
								<br>
								<p class="text-justify">
									Ofrecemos una gran variedad de productos y programas de nutrición que se ajustan a las necesidades de su operación ganadera, desde un programa de alimentación para una operación vaca becerro, hasta programas nutricionales específicos para mejorar la calidad de la leche o la carne.
									<br><br>
									Desde 1885, la tecnología desarrollada por Moorman's y adquirida por ADM y VIMIFOS ha creado productos de la más alta calidad nutricional. VIMIFOS aprovecha esta labor para complementar más de 100 años de investigación, con servicios y apoyos en el rancho para los productores, comprometidos en lograr el mayor beneficio posible de los recursos forrajeros, diseñando suplementos ideales para complementar las deficiencias nutricionales básicas del pasto en cada etapa del año y obteniendo mejores utilidades para su empresa ganadera.
								</p>
								<br><br><br>
								<small>
									El paquete de productos y servicios disponibles para la Ganadería están soportados por VIMIFOS con un excelente servicio técnico en las áreas de nutrición, manejo, sanidad, aseguramiento de calidad, reproducción y análisis de la información, así como también por asesores externos tanto nacionales como extranjeros.
								</small>
								<br>
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/bovino/introduccion.png"  class="pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="productos">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<div class="row">
									<div class="col-md-8">
										<h2 class="bovino-color bovino-title">
											<img src="/img/industrias/bovino/vaca.png" alt="">
											GANADO DE FINALIZACIÓN
										</h2>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/bovino/productos/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Suplemento científicamente formulado para ganado en pastoreo, diseñado para proveer vitaminas, minerales y proteinas.<br>
											<b class="bovino-color">Para temporada de secas.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/bovino/productos/2.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Suplemento mineral para contribuhir al rendimiento del ganado en pastoreo <br>
											<b class="bovino-color">Para temporada de lluvias.</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/bovino/productos/3.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Complemento para una buena nutrición de ganado aprovechando al máximo el agastadero.<br>
											<b class="bovino-color">Para ganado en pastoreo</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/bovino/productos/4.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Suplemento de proteina, energía, vitaminas y minerales; para ganado en pastoreo, cuando se requiera reforzar el aporte de nutrientes.<br>
											<b class="bovino-color">Más peso, más kilos, más utilidad</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/bovino/productos/5.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Alimento diseñado para cubrir los requerimientos nutricionales de los animales en crecimiento y desarrollo, después del desahije.<br>
											<b class="bovino-color">Fuente de proteínas de origen vegetal.</b>
										</p>
									</div>
								</div>

								<div class="row margin-top-15">
									<div class="col-md-3">
										<img src="/img/industrias/bovino/productos/6.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Alimento formulado para obtener y permitir la expresiónd e la genética, especialmente en vaquillas y toretes que serán destinadas a reproductores y transmisores de genes de un alto valor productivo.<br>
											<b class="bovino-color">Ganancias concretas en el desarrollo muscular y de células adiposas.</b>
										</p>
									</div>
								</div>
								
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/bovino/productos.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tecnologias">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="bovino-color">TECNOLOGÍAS</h5>
								<br>
								<p class="text-justify bovino-color">
									<b>
									Una de las grandes cualidades que diferencian a VIMIFOS es la utilización de tecnología exclusiva aplicada al desarrollo de la nutrición.
									</b>
								</p>

								<li class="bovino-color">Citristim:</li>
								<p class="text-justify">
									Oligasacarido de mananos (MOS) es un producto de levadura propietario de ADM que puede ser utilizado en todas las etapas de producción de ganado de leche. Los digasacáridos manamos han demostrado poder ser utilizados como secuestrantes de bacterias patógenas incluyendo E. Cali, Salmonelia, Proteus, Klebsuela y Clastridium.
								</p>

								<br><br>

								<li class="bovino-color">Bluret:</li>
								<p class="text-justify">
									Fuente de nitrógeno no proteínico (NNP) que se libera gradualmente en el resumen (proteína de lenta liberación). Debido a sus propiedades químicas únicas, el Bluret es liberado más lentamente en el rumen comparado con la urea y se mantiene disponible por un mayor periodo de tiempo.
								</p>

								<div class="row">	
									<div class="col-md-6">
										<img src="/img/industrias/bovino/tecnologia/1.png" class="center-block" alt="">
									</div>
									<div class="col-md-6">
										<img src="/img/industrias/bovino/tecnologia/2.png" class="center-block" alt="">
									</div>
								</div>
								

								<li class="bovino-color">Weathermaster:</li>
								<p class="text-justify">
									Estás siempre protegido, las indemencias del clima no tienen que ser una preocupación cuando tus suplementos minerales están protegidos. Exclusivo proceso de protección contra el clima de <b>VIMIFOS.</b>
								</p>
								<img src="/img/industrias/bovino/tecnologia/3.png" class=" margin-top-15" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/bovino/tecnologias.png"  class="img-100 pull-right" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="alianzas">
						<div class="row margin-top-15">
							<div class="col-md-8">
								
								<h5 class="bovino-color">ALIANZAS</h5>
								<br>
								<p class="text-justify bovino-color">
									<b>
									Contamos con alianzas estratégicas que nos ayudan a elaborar productos únicos que ayuden a obtener mayor productividad en los negocios de nuestros clientes.
									</b>
								</p>

								<li class="bovino-color">ADM:</li>
								<p class="text-justify">
									Archer Daniels Midland Company es uno de los más grandes procesadores agrícolas del mundo. Fundada en 1902 e incorporada en 1923. ADM tiene su sede en DECATUR, su operación es en todo el mundo a través de sus amplias instalaciones de distribución global. <br><br>
									ADM hace una contribución significativa a la economía mundial y la calidad de vida.
								</p>

								<img src="/img/industrias/porcicultura/adm.png" class="center-block margin-top-15" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/porcicultura/alianzas.png"  class="img-responsive pull-right" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


