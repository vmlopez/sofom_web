@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs tab-yellow" role="tablist">
					<li role="presentation" class="active">
						<a href="#avicultura" aria-controls="avicultura" role="tab" data-toggle="tab">AVICULTURA</a>
					</li>
					<li role="presentation">
						<a href="#introduccion" aria-controls="introduccion" role="tab" data-toggle="tab">INTRODUCCIÓN</a>
					</li>
					<li role="presentation">
						<a href="#productos" aria-controls="productos" role="tab" data-toggle="tab">PRODUCTOS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="avicultura">
						<div class="row margin-top-15">
							<div class="col-md-12">
								<img class="center-block" src="/img/industrias/avicultura/tab-1.png" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="introduccion">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="yellow">INTRODUCCIÓN</h3> <br>
								<p class="text-justify yellow">
									<b>Nuestros especialistas en Avicultura tienen el compromiso de trabajar en conjunto con los productos avícolas nacionales teniendo como objetivo primordial la optimización de los parámetros productivos a través de una nutrición eficiente y un servicio de excelencia.</b> 
								</p>
								<br>
								<p class="text-justify">
									Otro objetivo es el de asesorar y facilitar las actividades relacionadas con la producción de alimentos balanceados. Por lo anterior, contamos con diferentes tipos de productos dependiendo de la forma en que las plantas de alimentos estén organizadas en su proceso productivo.
								</p>
								<br>
								<p class="text-justify">
									Todos los productos están elaborados en las dosis necesarias para cubrir los requerimientos de cada fase de alimentación.
								</p>
								<br>
								<ul>
									<li>
										<b class="yellow">Seguridad</b> <br>
										Diseñamos productos utilizando la experiencia y el conocimiento que tenemos en la nutrición
									</li>
									<li>
										<b class="yellow">Presición</b> <br>
										Servicios en la producción avícola para generar mejores resultados.
									</li>
									<li>
										<b class="yellow">Economía</b> <br>
										Mejores resultados positivos, basados en una relación rentable de costo-beneficio
									</li>
								</ul>
								<br>
								<p class="text-justify">
									Los resultados productivos obtenidos, así como la satisfacción en los servicios ofrecidos son responsabilidad de VIMIFOS.
								</p>
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/avicultura/tab-2.png"  class="img-responsive" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="productos">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<div class="row margin-top-20">
									<div class="col-md-3">
										<img src="/img/industrias/avicultura/producto-1.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											<b>Premezclas.</b> Premezclar es el proceso de elaboración de alimentos de vitaminas y minerales, es la etapa más importante que constituye la clave para proveer através del alimento, los nutrientes necesarios para la obtención de los resultados productivos buscados. <br>
											<b class="yellow">Premezclas especializadas de acuerdo a tus necesidades</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-20">
									<div class="col-md-3">
										<img src="/img/industrias/avicultura/opti-b.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											<b>Opticolor B</b> Es un aditivo alimenticio que contiene una mezcla de xantofilas destinadas a mejorar la apariencia de color de la piel del pollo e engorda <br>
											<b class="yellow">Aditivos para mejorar el aspecto de la piel</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-20">
									<div class="col-md-3">
										<img src="/img/industrias/avicultura/opti-e.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											<b>Opticolor E</b> Es un aditivo alimenticio que contiene una mezcla de xantofilas destinadas a mejorar la apariencia de color de la yema del huevo <br>
											<b class="yellow">Aditivos para mejorar el aspecto de la yema del huevo</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-20">
									<div class="col-md-3">
										<img src="/img/industrias/avicultura/pro-hatch.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											<b>Prohactch</b> Es un aditivo que contiene mezcla de productos de levaduras y aceites esenciales, destinados a mejorar la integridad intestinal y la inmunidad en aves reproductoras pesadas o ligeras.<br>
											<b class="yellow">Mejora la salud intestinal y el sistema Inmune</b>
										</p>
									</div>
								</div>
								<div class="row margin-top-20">
									<div class="col-md-3">
										<img src="/img/industrias/avicultura/oxi-gea.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											<b>Oxigea</b> Es una combinación de antioxidantes, provitaminas, probióticos para uso en pollo de engorda y gallinas de pastura que mejora la productividad de las aves en condiciones de estrés, intoxicación, problemas infecciosos y/o metabólicos. <br>
											<b class="yellow">Mejora la productividad.</b>
										</p>
									</div>
								</div>
								
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/avicultura/tab-3.png"  class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


