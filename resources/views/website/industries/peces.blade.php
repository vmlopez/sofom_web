@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs " role="tablist">
					<li role="presentation" class="active">
						<a href="#peces" aria-controls="peces" role="tab" data-toggle="tab">PECES</a>
					</li>
					<li role="presentation">
						<a href="#introduccion" aria-controls="introduccion" role="tab" data-toggle="tab">INTRODUCCIÓN</a>
					</li>
					<li role="presentation">
						<a href="#productos" aria-controls="productos" role="tab" data-toggle="tab">PRODUCTOS</a>
					</li>
					<li role="presentation">
						<a href="#tecnologias" aria-controls="tecnologias" role="tab" data-toggle="tab">TECNOLOGÍAS</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="peces">
						<div class="row margin-top-15">
							<div class="col-md-12">
								
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="introduccion">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h3 class="blue">INTRODUCCIÓN</h3> <br>
								<p class="text-justify blue">
									<b>Alimentos diseñados para proveer la totalidad de los nutrientes requeridos en sus distintas etapas de desarrollo, en cualquier sistema de cultivo.</b> 
								</p>
								<br>
								<p class="text-justify">
									Las dietas contienen ingredientes altamente digestibles y palatables, que aportan los nutrientes necesarios para una óptima salud, y  en sus distintas presentaciones pueden ser empleadas para alimentar tilapias desde su nacimiento hasta talla de mercado. <br><br>	
									Se ha comprobado que los ingredientes de VIMIFOS tienen un impacto positivo en el crecimiento y  salud del animal porque inhibe la proliferación de bacterias patógenas en el intestino. Promueve y refuerza el sistema inmune de los peces, ayudando a afrontar situaciones de estrés experimentadas prácticas comunes de cultivos y mejorando los niveles de sobrevivencia. <br><br>	
									Todos los productos están elaborados en las dosis necesarias para cubrir los requerimientos de cada fase de alimentación, ofreciendo las siguientes ventajas:
								</p>
								<br>
								<ul>
									<li>
										<b class="blue">Alta digestibilidad:</b> <br>
										Óptima conversión alimenticia. Máximo aprovechamiento de los nutrientes con una generación  mínima de residuos.
									</li>
									<li>
										<b class="blue">Nutrientes balanceados:</b> <br>
										Contienen los aminoácidos, ácidos grasos, contenido energético y todas las  vitaminas y minerales requeridos en las diversas etapas de desarrollo de los peces. Uso eficiente de los nutrientes administrados, maximizando  eficiencia del alimento.
									</li>
									<li>
										<b class="blue">Alta palatabilidad:</b> <br>
										Rápido consumo y máximo aprovechamiento del alimento.
									</li>
								</ul>
								<br>
								<p class="text-justify">
									El paquete de productos y servicios disponibles para la Tilapia están soportados por VIMIFOS con un excelente servicio técnico en las áreas de nutrición, manejo, sanidad, aseguramiento de calidad, reproducción y análisis de la información, así como también por asesores externos tanto nacionales como extranjeros.
								</p>
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/peces/introduccion.png"  class="img-100" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="productos">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<div class="row margin-top-20">
									<div class="col-md-3">
										<img src="/img/industrias/peces/productos-2.png" class="center-block" alt="">
									</div>
									<div class="col-md-9">
										<p class="text-justify">
											Dietas científicamente formuladas para ser usadas como alimento completo para Tilapia, que permiten maximizar la sobrevivencia, la velocidad de crecimiento y la calidad del producto con una reducida conversión alimenticia. <br><br>	
											<b class="blue">	Permite alcanzar el mejor costo beneficio. </b>
										</p>

									</div>
								</div>
								<div class="row">	
									<div class="col-md-10 col-md-offset-1">	
										<img src="/img/industrias/peces/productos-1.png" class="img-100 center-block" alt="">
									</div>
								</div>
								
								
								
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/peces/productos.png"  class="img-100 center-block" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="tecnologias">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4 class="blue">Citristim, promueve y refuerza el sistema inmune.</h4> <br><br>
								<li class="blue">Citristim:</li>
								
								<p class="text-justify">
									Una de las grandes cualidades que diferencian a VIMIFOS es la utilización de tecnología exclusiva aplicada al desarrollo de la nutrición para peces en sus distintas etapas de desarrollo, en cualquier sistema de cultivo. <br><br>
									CitriStim, un oligosacárido de mananos (MOS), es un producto de levadura propietario de ADM que puede ser utilizado en las dietas de peces, en recepción para incrementar el crecimiento. Los oligosacáridos mananos han demostrado poder ser utilizados como secuestrantes de bacterias patógenas incluyendo E. Coli, Salmonella, Proteus, Klebsiella y Clostridum.
								</p>
								
								<img src="/img/industrias/peces/tecnologias-2.png"  class="img-100 center-block" alt="">
							</div>
							<div class="col-md-4">
								<img src="/img/industrias/peces/tecnologias.png"  class="img-100 center-block" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


