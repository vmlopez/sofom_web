@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')

    <div class="container">


        <div class="row">

            <div class="col-md-12">

                <h4>CAT</h4>

                <br>

                <p class="blue">
                    Información del CAT al público.
                </p>
                <p>

                    Costo Anual Total (CAT Promedio) = 18.3% Sin IVA. Para fines informativos y de comparación exclusivamente.
                    Cálculo realizado al 12 de diciembre del 2017, considerando una disposición de $ 1'000,000.00 MN con plazo de 180 días
                    y una tasa de interés de 15.40%. La información mostrada tiene únicamente fines ilustrativos, por lo que no implica
                    asunción de obligación ni compromiso por parte de Corporativo Financiero Vimifos, S. A. de C. V. SOFOM E. N. R.


                </p>

            </div>


        </div>
    </div>

@endsection



@section('footer')
    @parent
@endsection

@section('scripts')



@endsection


