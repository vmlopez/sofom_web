@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">

		
		<div class="row">
			
			<div class="col-md-12">

				<!-- Tabs login -->
				<ul class="nav nav-tabs " role="tablist">
					<li role="presentation" class="active">
						<a href="#distribuidores" aria-controls="distribuidores" role="tab" data-toggle="tab">SOLUCIONES</a>
					</li>
					<li role="presentation">
						<a href="#redes" aria-controls="redes" role="tab" data-toggle="tab">FINANCIAMIENTO</a>
					</li>
					<li role="presentation">
						<a href="#distribuidor" aria-controls="distribuidor" role="tab" data-toggle="tab">BENEFICIOS</a>
					</li>
					<li role="presentation">
						<a href="#contacto" aria-controls="contacto" role="tab" data-toggle="tab">ACCESIBLE</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 " id="distribuidores">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4 >
									SOLUCIONES
								</h4>
								<h4 class="blue text-justify">
									Por medio de variados instrumentos de crédito VIMIFOS CAPITAL te ofrece lo que tu empresa necesita.
									Soluciones para hacer crecer tu negocio.
								</h4>
								<br><br>

								<p class="text-justify">
									A diferencia de las instituciones de financiamiento tradicionales de México, buscamos involucrarnos
									de manera profunda en tu negocio con el objetivo de detectar tus necesidades reales y poder ofrecerles
									soluciones definitivas a los problemas que enfrentan cotidianamente.
									<br><br>
									La experiencia y conocimiento del mercado nos respalda  desde hace ya más de tres décadas, a través
									de la venta de nuestros productos VIMIFOS, lo que nos permite conocer a fondo las necesidades específicas
									de los productores, y en base a eso crear soluciones financieras diseñadas especialmente para cada uno.
									<br><br>
									Nuestro compromiso es ofrecer mayor agilidad, entendiendo el valor del tiempo y por ello, nos preocupamos
									en  mejorar sustancialmente los tiempos de respuesta para cualquier solicitud.
								</p>


							</div>
							<div class="col-md-4">
								<img src="/img/distribuidores/banner-soluciones.jpg"  class="img-responsive" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="redes">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4 class="blue">FINANCIAMIENTO</h4>
								<h4 class="blue">
									Un verdadero crédito a la medida. Sabemos que cada negocio es muy particular y opera de forma diferente,
									por lo que creamos financiamientos a la medida de tus necesidades.
								</h4>
								<br>

								<h4 class="blue">CAPITAL DE TRABAJO</h4>
								<p>
									Son aquellos créditos y financiamiento que se utilizan para dar liquidez a una empresa o actividad
									ya sea productiva o comercial (habilitación o avío, reportos, etc).
								</p>
								<br><br>

								<table class="table table-bordered table-striped">
									<thead>
									<tr>
										<th class="text-center bg-blue">PRODUCTO</th>
										<th class="text-center bg-blue">DESCRIPCIÓN</th>
									</tr>
									</thead>
									<tbody>
									<tr>
										<td class="text-center bg-blue">Crédito simple</td>
										<td class="text-center">Proporcionar fondos para adquisición de bienes y servicios.</td>
									</tr>
									<tr>
										<td class="text-center bg-blue">Crédito Habilitación / Avio</td>
										<td class="text-center">Destinado para compra de cualquier componente del capital de trabajo.</td>
									</tr>
									<tr>
										<td class="text-center bg-blue">Crédito Refaccionario</td>
										<td class="text-center">Destinado a fortalecer o incrementar activos fijos.</td>
									</tr>
									<tr>
										<td class="text-center bg-blue">Cuenta corriente</td>
										<td class="text-center">Financiamiento para el capital de trabajo con disposición revolvente del saldo disponible.</td>
									</tr>
									<tr>
										<td class="text-center bg-blue">Reporto</td>
										<td class="text-center">
											Financiamiento a corto plazo de los inventarios del productor contra el endoso
											en propiedad de certificados de depósito y bonos de prenda emitidos por un almacén general de depósito.
										</td>
									</tr>
									</tbody>
								</table>

								<p>
									REQUISITOS Y CONDICIONES. <br>
									Edad de 18 A 65 años. <br>
									Tipo de persona física con Act. Empr y Persona Moral. <br>
									Antigüedad operación 1 año. <br>
									Buen historial crediticio. <br>
									Aval y garantía hipotecaria o prendaria. <br>
									Solicitud de crédito. <br>
									Información financiera, Estado de cuenta. <br>
									Documentos de identificación. <br>
									Sujeto a autorización de crédito.* <br><br>
									COMISIONES <br>
									3% Comisión Máxima por disposición. <br>
								</p>

								<h4 class="blue">INVERSIONES ACTIVO FIJO</h4>
								<br>
								<p>
									Son aquellos créditos que se utilizan para mejorar la capacidad productiva de las empresas.
									Financiamos activos fijos que permitan continuar o incremetar la actividad económica y que
									tengan un impacto en la generación del flujo de efectivo, ya sea porque incrementan los ingresos
									o por un ahorro en costos (refaccionarios). <br><br>
									<b>Información del CAT al público. </b> <br><br>
									<b>Costo Anual Total (CAT Promedio) = 18.3% Sin IVA. Para fines informativos y de comparación exclusivamente.
										Cálculo realizado al 12 de Diciembre de 2017, considerando una disposición de $ 1'000,000.00 MN con plazo de 180 días
										y una tasa de interés de 15.40%. La información mostrada tiene únicamente fines ilustrativos, por lo que no implica
										asunción de obligación ni compromiso por parte de Corporativo Financiero Vimifos, S. A. de C. V. SOFOM E. N. R.
									</b>
								</p>

							</div>
							<div class="col-md-4">
								<img src="/img/distribuidores/banner-financiamiento.jpg"  class="img-responsive" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="distribuidor">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4 >BENEFICIOS</h4>
								<h4 class="blue">
									<b>
										Nos gusta conocer a fondo el negocio de nuestros clientes, con el objetivo de ofrecerles la
										opción de financiamiento que se amolde a tus necesidades.
									</b>
								</h4>
								<br><br>
								<p class="text-justify">
									Trabajando en conjunto para exponer de forma real tu negocio y encontrar soluciones a
									los problemas financieros con los que conviven a diario, ofreciendo los mejores beneficios para tu empresa.
								</p>
								<br><br>
								<h4 class="blue">Nuestros beneficios:</h4>
								<h4><b>Para solicitar una distribución VIMIFOS es necesario:</b></h4>
								<ul class="text-justify">
									<li>Brindamos asesoría gracias a la experiencia ganada durante los más de 35 años dentro de la industria pecuaria y acuícola.</li>
									<li>Otorgamos créditos a la medida de tus necesidades y de tu ciclo productivo.</li>
									<li>Apoyamos la inversión en activos productivos que les permitan obtener mejores parámetros de producción.</li>
									<li>Contar con mayor liquidez, realizar mejores compras y planear mejor tus ciclos.</li>
									<li>Diseñamos una estructura de pago definida por el histórico de cada negocio para que realices pagos adecuados en los momentos oportuno según tu flujo de efectivo.</li>
								</ul>
							</div>
							<div class="col-md-4">
								<img src="/img/distribuidores/banner-beneficios.png"  class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane padding-5 " id="contacto">
						<div class="row margin-top-15">
							<div class="col-md-8">
								<h4>
									ACCESIBLE
								</h4>

								<p class="text-justify blue">
									Nuestros clientes cumplen con sus obligaciones ante las autoridades y cuentan con todos los
									lineamientos de una administración formal, por ello buscamos facilitar el trámite, siendo
									lo más simple, favorable y accesibles, apoyándolos en todo lo necesario para poder ser un
									cliente potencial para VIMIFOS CAPITAL.
								</p>

								<p class="blue">Requisitos:</p>
								<ul class="text-justify">
									<li>Tener operaciones con una antiguedad mayor a dos años.</li>
									<li>Contar con administración formal, estar inscritos ante SHCP y al corriente de las obligaciones fiscales.</li>
									<li>Integrar al 100% la documentación requerida.</li>
									<li>Que se cubran los gastos de avalúo y contratación.</li>
									<li>Qué esté de acuerdo con las condiciones financieras, aprobadas por el comité de crédito.</li>
									<li>Disponibilidad para otorgar garantía hipotecaria.</li>
								</ul>

							</div>
							<div class="col-md-4">
								<img src="/img/distribuidores/banner-accesible.jpg"  class="img-responsive center-block" alt="">
							</div>
						</div>
					</div>
				</div>

			</div>
			

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


