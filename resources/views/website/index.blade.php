@extends('layouts.website')

@section('title', 'Page Title')

@section('navbar')
    @parent
@endsection

@section('content')
	
	<div class="container">
			
		<div id="home-carousel" class="carousel slide" data-ride="carousel">
			<!-- Indicators -->
			<ol class="carousel-indicators">
				<li data-target="#home-carousel" data-slide-to="0" class="active"></li>
				<li data-target="#home-carousel" data-slide-to="1"></li>
				<li data-target="#home-carousel" data-slide-to="2"></li>
			</ol>

			<!-- Wrapper for slides -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img class="center" src="img/slide/1.jpg" alt="...">
				</div>
				<div class="item">
					<img class="center" src="img/slide/2.jpg" alt="">
				</div>
				<div class="item">
					<img class="center" src="img/slide/3.jpg" alt="">
				</div>
			</div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#home-carousel" role="button" data-slide="prev">
		    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="right carousel-control" href="#home-carousel" role="button" data-slide="next">
		    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
		
		<div class="row">
			
			<div class="col-md-5">

				<!-- Tabs login -->
				<ul class="nav nav-tabs " role="tablist">
					<li role="presentation" class="active">
						<a href="#login-cliente" aria-controls="login-cliente" role="tab" data-toggle="tab">Cliente / Distribuidor</a>
					</li>
					<li role="presentation">
						<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Colaborador</a>
					</li>
				</ul>

				<!-- Contenido de las tabs login -->
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active padding-5 bg-blue" id="login-cliente">
						<div class="row margin-top-15">
							<div class="col-md-6">
								<h5 class="text-center">
									UN PORTAL DEDICADO A NUESTROS CLIENTES Y DISTRIBUIDORES
								</h5>
							</div>
							<div class="col-md-6 ">
								<div class="form-group ">
									<input type="text" class="form-control no-radius" placeholder="Usuario">
								</div>
								<div class="form-group">
									<div class="input-group">
										<input type="password" class="form-control no-radius" placeholder="Contraseña" aria-describedby="button-login">
										<span class="input-group-btn" id="button-login">
											<button class="btn btn-go no-radius">Go</button>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div role="tabpanel" class="tab-pane" id="profile">...</div>
				</div>

			</div>
			<div class="col-md-4">
				<img src="img/index/distribuidor.jpg" class="center-block margin-top-25" alt="">
			</div>
			<div class="col-md-3">
				<img src="img/index/detalles.jpg" class="center-block margin-top-25" alt="">
			</div>

		</div>
	</div>

@endsection



@section('footer')
	@parent
@endsection

@section('scripts')
	
	

@endsection


