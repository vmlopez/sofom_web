<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/css/app.css">
	
	@yield('style')

	<title>Vimifos</title>
</head>
<body>
	
	@section('navbar')

	<div class="container bg-white">
		<div class="row">
			<div class="col-md-5 padding-5">
				
				<a class="blue " href="/">
					<img class="img-logo" src="/img/logo.png" alt="">
				</a>
			</div>
			<div class="col-md-7 padding-5 margin-top-10">
				<div class="row">
					<div class="col-md-12 ">
						<div class="pull-right">
							<a href="/gana-mas?contacto=1">
								<span class="glyphicon glyphicon-envelope"></span>
								Contacto
							</a>
						</div>
					</div>
					<div class="col-md-12 margin-top-10">
						<div class="pull-right">
							<div class="row">

								<div class="col-md-12">
									<div class="row">

										<div class="col-md-6">
											<input type="text" class="form-control rounded" placeholder="Buscar">
										</div>
										<div class="col-md-6">
											<a href="/pdf/VimifosCapitalPDF.pdf" target="_blank">
												<img src="/img/boton_descarga.jpg" class="pull-right" alt="">
											</a>
										</div>
									</div>
								</div>
								
							</div>
						</div>
					</div>
					
				</div>
				
			</div>
		</div>

	</div>
	<nav class="navbar navbar-default">
		<div class="container">

	    	<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse-nav" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="collapse-nav">

				<ul class="nav navbar-nav">
					<li ><a class="margin-5" href="/">INICIO </a></li>

					<li ><a class="margin-5" href="/nosotros">NOSOTROS</a></li>

					<li ><a class="margin-5" href="/soluciones">SOLUCIONES </a></li>
					<li ><a class="margin-5" href="/gana-mas">GANA MÁS </a></li>
					{{--<li ><a class="margin-5" href="/formatos">FORMATOS</a></li>--}}
				</ul>
				<ul class="nav navbar-nav navbar-right no-padding">					
					<li>
						
					</li>
				</ul>

		    </div><!-- /.navbar-collapse -->

		</div><!-- /.container -->

	</nav>
	@show
	
	@yield('content')
	
	@section('footer')
	<footer class="margin-top-20 footer">
		<div class="container bg-footer">

			<div class="row">
				
				<div class="col-md-6 margin-top-20 margin-bottom-20">
					<a href="/regulaciones" class="font-size-10">REGULACIONES</a> <br>
					<a href="/cat" class="font-size-10">CAT</a> <br>
					<a href="/atencion" class="font-size-10">ATENCIÓN A USUARIOS</a> <br>
					<a href="/buro-entidades-financieras" class="font-size-10">BURO DE ENTIDADES FINANCIERAS</a> <br>
					<a href="/aviso-de-privacidad" class="font-size-10">AVISO DE PRIVACIDAD</a> <br>
					<a href="" class="font-size-10">TODOS LOS DERECHOS RESERVADOS VIMIFOS 2012</a>
					<a href="" class="font-size-10">CORPORATIVO VIMIFOS</a>

				</div>
				<div class="col-md-6 margin-top-10">
					<img src="/img/footer.png" class="pull-right" alt="">
				</div>

			</div>

		</div>
		
	</footer>
	@show

	<script src="/js/app.js"></script>
	<script src="/js/bootstrap-dropdown-hover.min.js"></script>

	@yield('scripts')

</body>
</html>